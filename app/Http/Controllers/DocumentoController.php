<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Documento;
use DB;
class DocumentoController extends Controller
{
		protected $imagePath = '/assets/documentos/POC/';

    public function store(Request $request)
    {
        $data = $request->all();
				if($data['departamento_id'] == 1)
				{
					$path = $request->file('file')->storeAs('/public/assets/documentos/POC/', $request->file('file')->getClientOriginalName());
					$file = $request->file('file');
					$nombre = $file->getClientOriginalName();	
					$documento                = new Documento();
					$documento->nombre  = 						 $data['nombre'];
					$documento->ruta  = 							 '/assets/documentos/POC/'.$nombre;
					$documento->departamento_id  = 	 $data['departamento_id'];
					$documento->subdepartamento_id  = $data['subdepartamento_id'];
					
					$documento->save();
					return redirect('/admin');
				}
				elseif($data['departamento_id'] == 2)
				{
					$path = $request->file('file')->storeAs('/public/assets/documentos/AC/', $request->file('file')->getClientOriginalName());
					$file = $request->file('file');
					$nombre = $file->getClientOriginalName();	
					$documento                = new Documento();
					$documento->nombre  = 						 $data['nombre'];
					$documento->ruta  = 							 '/assets/documentos/AC/'.$nombre;
					$documento->departamento_id  = 	 $data['departamento_id'];
					$documento->subdepartamento_id  = $data['subdepartamento_id'];
					
					$documento->save();
					return redirect('/admin');
				}
				
		
				elseif($data['departamento_id'] == 3)
				{
					$path = $request->file('file')->storeAs('/assets/documentos/CO/', $request->file('file')->getClientOriginalName());
					$file = $request->file('file');
					$nombre = $file->getClientOriginalName();	
					$documento                = new Documento();
					$documento->nombre  = 						 $data['nombre'];
					$documento->ruta  = 							 '/assets/documentos/CO/'.$nombre;
					$documento->departamento_id  = 	 $data['departamento_id'];
					$documento->subdepartamento_id  = $data['subdepartamento_id'];
					
					$documento->save();
					return redirect('/admin');
				}
				elseif($data['departamento_id'] == 4)
				{
					$path = $request->file('file')->storeAs('/assets/documentos/RH/', $request->file('file')->getClientOriginalName());
					$file = $request->file('file');
					$nombre = $file->getClientOriginalName();	
					$documento                = new Documento();
					$documento->nombre  = 						 $data['nombre'];
					$documento->ruta  = 							 '/assets/documentos/RH/'.$nombre;
					$documento->departamento_id  = 	 $data['departamento_id'];
					$documento->subdepartamento_id  = $data['subdepartamento_id'];
					
					$documento->save();
					return redirect('/admin');
				}
				elseif($data['departamento_id'] == 5)
				{
					$path = $request->file('file')->storeAs('/assets/documentos/VEN/', $request->file('file')->getClientOriginalName());
					$file = $request->file('file');
					$nombre = $file->getClientOriginalName();	
					$documento                = new Documento();
					$documento->nombre  = 						 $data['nombre'];
					$documento->ruta  = 							 '/assets/documentos/VEN/'.$nombre;
					$documento->departamento_id  = 	 $data['departamento_id'];
					$documento->subdepartamento_id  = $data['subdepartamento_id'];
					
					$documento->save();
					return redirect('/admin');
				}
				elseif($data['departamento_id'] == 6)
				{
					$path = $request->file('file')->storeAs('/assets/documentos/PRF/', $request->file('file')->getClientOriginalName());
					$file = $request->file('file');
					$nombre = $file->getClientOriginalName();	
					$documento                = new Documento();
					$documento->nombre  = 						 $data['nombre'];
					$documento->ruta  = 							 '/assets/documentos/PRF/'.$nombre;
					$documento->departamento_id  = 	 $data['departamento_id'];
					$documento->subdepartamento_id  = $data['subdepartamento_id'];
					
					$documento->save();
					return redirect('/admin');
				}
				elseif($data['departamento_id'] == 7)
				{
					$path = $request->file('file')->storeAs('/assets/documentos/DSP/', $request->file('file')->getClientOriginalName());
					$file = $request->file('file');
					$nombre = $file->getClientOriginalName();	
					$documento                = new Documento();
					$documento->nombre  = 						 $data['nombre'];
					$documento->ruta  = 							 '/assets/documentos/DSP/'.$nombre;
					$documento->departamento_id  = 	 $data['departamento_id'];
					$documento->subdepartamento_id  = $data['subdepartamento_id'];
					
					$documento->save();
					return redirect('/admin');
				}
				elseif($data['departamento_id'] == 8)
				{
					$path = $request->file('file')->storeAs('/assets/documentos/HEM/', $request->file('file')->getClientOriginalName());
					$file = $request->file('file');
					$nombre = $file->getClientOriginalName();	
					$documento                = new Documento();
					$documento->nombre  = 						 $data['nombre'];
					$documento->ruta  = 							 '/assets/documentos/HEM/'.$nombre;
					$documento->departamento_id  = 	 $data['departamento_id'];
					$documento->subdepartamento_id  = $data['subdepartamento_id'];
					
					$documento->save();
					return redirect('/admin');
				}
				elseif($data['departamento_id'] == 9)
				{
					$path = $request->file('file')->storeAs('/assets/documentos/COVID/', $request->file('file')->getClientOriginalName());
					$file = $request->file('file');
					$nombre = $file->getClientOriginalName();	
					$documento                = new Documento();
					$documento->nombre  = 						 $data['nombre'];
					$documento->ruta  = 							 '/assets/documentos/COVID/'.$nombre;
					$documento->departamento_id  = 	 $data['departamento_id'];
					$documento->subdepartamento_id  = $data['subdepartamento_id'];
					
					$documento->save();
					return redirect('/admin');
				}

						

			
		

    }

		public function search($busqueda)
    {

		$data = DB::table('departamentos as d')
		->join('documentos as c', 'd.departamento_id', '=', 'c.departamento_id')
		->leftJoin('subdepartamentos as s','c.subdepartamento_id','=','s.subdepartamento_id')
		->select('c.nombre as doc_name','c.ruta','c.created_at', 'd.nombre as dep_name','s.nombre as sub_name')
		->Where('c.nombre', 'like', '%' . $busqueda . '%')
		->orWhere('c.ruta', 'like', '%' . $busqueda . '%')
		->orWhere('d.nombre', 'like', '%' . $busqueda . '%')
		->orWhere('s.nombre', 'like', '%' . $busqueda . '%')
		->get();
       
        
		return [
			'status'  => 201,
			'message' => 'Documento registrada con éxito',
			'data'    => compact('data')
		];

    }

	public function open() 
	{
		$data = "This data is open and can be accessed without the client being authenticated";
		return response()->json(compact('data'),200);

	}

	public function closed() 
	{
		$data = "Only authorized users can see this";
		return response()->json(compact('data'),200);
	}

	public function showDocumentFilter($id)
    {
        $documentos=Documento::where('documento_id',$id)->first();

            return [
            'status' => 200,
            'message'=>'Lista de documentos',
            'data' => compact('documentos'),
        ];

    }

	public function updatedoc (Request $request)
	{
		$data = $request->all();
		$documento                       = Documento::find($data['documento_id_up']);
		$documento->nombre  = 						 $data['nombre_up'];
		$documento->departamento_id  = 	 $data['departamento_id'];
		if($request->file('ruta') != null)
		{
			if($data['departamento_id'] == 1)
			{
				$path = $request->file('ruta')->storeAs('public/assets/documentos/POC/', $request->file('ruta')->getClientOriginalName());
				$file = $request->file('ruta');
				$nombre = $file->getClientOriginalName();	
			
			
				$documento->ruta  = 							 '/assets/documentos/POC/'.$nombre;
				
			}
			elseif($data['departamento_id'] == 2)
				{
					$path = $request->file('ruta')->storeAs('/assets/documentos/AC/', $request->file('ruta')->getClientOriginalName());
					$file = $request->file('ruta');
					$nombre = $file->getClientOriginalName();	

					$documento->ruta  = 							 '/assets/documentos/AC/'.$nombre;

				}
				elseif($data['departamento_id'] == 3)
				{
					$path = $request->file('ruta')->storeAs('/assets/documentos/CO/', $request->file('ruta')->getClientOriginalName());
					$file = $request->file('ruta');
					$nombre = $file->getClientOriginalName();	
					$documento                = new Documento();
				
					$documento->ruta  = 							 '/assets/documentos/CO/'.$nombre;
				
				}
				elseif($data['departamento_id'] == 4)
				{
					$path = $request->file('ruta')->storeAs('/assets/documentos/RH/', $request->file('ruta')->getClientOriginalName());
					$file = $request->file('ruta');
					$nombre = $file->getClientOriginalName();	
					$documento                = new Documento();
					
					$documento->ruta  = 							 '/assets/documentos/RH/'.$nombre;
			
				}
				elseif($data['departamento_id'] == 5)
				{
					$path = $request->file('ruta')->storeAs('/assets/documentos/VEN/', $request->file('ruta')->getClientOriginalName());
					$file = $request->file('ruta');
					$nombre = $file->getClientOriginalName();	
					$documento                = new Documento();
					
					$documento->ruta  = 							 '/assets/documentos/VEN/'.$nombre;

				}
				elseif($data['departamento_id'] == 6)
				{
					$path = $request->file('ruta')->storeAs('/assets/documentos/PRF/', $request->file('ruta')->getClientOriginalName());
					$file = $request->file('ruta');
					$nombre = $file->getClientOriginalName();	
					$documento->ruta  = 							 '/assets/documentos/PRF/'.$nombre;
				}
				elseif($data['departamento_id'] == 7)
				{
					$path = $request->file('ruta')->storeAs('/assets/documentos/DSP/', $request->file('ruta')->getClientOriginalName());
					$file = $request->file('ruta');
					$nombre = $file->getClientOriginalName();	
					$documento->ruta  = 							 '/assets/documentos/DSP/'.$nombre;
				}
				elseif($data['departamento_id'] == 8)
				{
					$path = $request->file('ruta')->storeAs('/assets/documentos/HEM/', $request->file('ruta')->getClientOriginalName());
					$file = $request->file('ruta');
					$nombre = $file->getClientOriginalName();	
					$documento->ruta  = 							 '/assets/documentos/HEM/'.$nombre;
				}
				elseif($data['departamento_id'] == 9)
				{
					$path = $request->file('ruta')->storeAs('/assets/documentos/COVID/', $request->file('ruta')->getClientOriginalName());
					$file = $request->file('ruta');
					$nombre = $file->getClientOriginalName();	
					$documento->ruta  = 							 '/assets/documentos/COVID/'.$nombre;
				}
		}
		else {
			$documento->save();
		return redirect('/admin');
		}

	
		$documento->save();
		return redirect('/admin');
	
	}
}
