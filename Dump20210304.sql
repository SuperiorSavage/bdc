-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: bdc
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `departamento_subdepartamento`
--

DROP TABLE IF EXISTS `departamento_subdepartamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departamento_subdepartamento` (
  `departamento_subdepartamento_id` int(11) NOT NULL AUTO_INCREMENT,
  `departamento_id` int(11) DEFAULT NULL,
  `subdepartamento_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`departamento_subdepartamento_id`),
  KEY `fk_departamentoi_id_ds1_idx` (`departamento_id`),
  KEY `fk_departamentoi_id_ds2_idx` (`subdepartamento_id`),
  CONSTRAINT `fk_departamentoi_id_ds1` FOREIGN KEY (`departamento_id`) REFERENCES `departamentos` (`departamento_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_departamentoi_id_ds2` FOREIGN KEY (`subdepartamento_id`) REFERENCES `subdepartamentos` (`subdepartamento_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departamento_subdepartamento`
--

LOCK TABLES `departamento_subdepartamento` WRITE;
/*!40000 ALTER TABLE `departamento_subdepartamento` DISABLE KEYS */;
INSERT INTO `departamento_subdepartamento` VALUES (1,1,1,'2021-02-11 05:56:59','2021-02-11 05:56:59',NULL),(2,1,2,'2021-02-11 05:59:51','2021-02-11 05:59:51',NULL),(3,1,3,'2021-02-11 06:00:41','2021-02-11 06:00:41',NULL),(4,1,4,NULL,NULL,NULL),(5,1,5,NULL,NULL,NULL),(6,1,6,NULL,NULL,NULL),(7,1,7,NULL,NULL,NULL),(8,1,8,NULL,NULL,NULL),(9,1,9,NULL,NULL,NULL),(10,1,10,NULL,NULL,NULL),(11,1,11,NULL,NULL,NULL),(12,1,12,NULL,NULL,NULL),(13,1,13,NULL,NULL,NULL),(14,3,14,NULL,NULL,NULL),(15,3,15,NULL,NULL,NULL),(16,3,16,NULL,NULL,NULL),(17,4,17,NULL,NULL,NULL),(18,4,18,NULL,NULL,NULL);
/*!40000 ALTER TABLE `departamento_subdepartamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departamentos`
--

DROP TABLE IF EXISTS `departamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departamentos` (
  `departamento_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`departamento_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departamentos`
--

LOCK TABLES `departamentos` WRITE;
/*!40000 ALTER TABLE `departamentos` DISABLE KEYS */;
INSERT INTO `departamentos` VALUES (1,'Procedimientos Operativos','2021-02-10 23:40:45','2021-02-10 23:40:45',NULL),(2,'Área Común','2021-02-10 23:48:53','2021-02-10 23:48:53',NULL),(3,'Contratos','2021-02-11 22:28:43','2021-02-11 22:28:43',NULL),(4,'Recursos Humanos','2021-02-11 22:28:57','2021-02-11 22:28:57',NULL),(5,'Ventas','2021-02-11 22:29:05','2021-02-11 22:29:05',NULL),(6,'Perfiles','2021-02-11 22:29:05','2021-02-11 22:29:05',NULL),(7,'Descripción de Puestos','2021-02-11 22:29:05','2021-02-11 22:29:05',NULL),(8,'Hemeroteca Vivir Bien','2021-02-11 22:29:05','2021-02-11 22:29:05',NULL),(9,'COVID-19','2021-02-11 22:29:05','2021-02-11 22:29:05',NULL);
/*!40000 ALTER TABLE `departamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentos`
--

DROP TABLE IF EXISTS `documentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentos` (
  `documento_id` int(11) NOT NULL AUTO_INCREMENT,
  `ruta` text,
  `nombre` text,
  `departamento_id` int(11) DEFAULT NULL,
  `subdepartamento_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`documento_id`),
  KEY `fk_departamento_id_doc_idx` (`departamento_id`),
  KEY `fk_subdepartamento_id_doc_idx` (`subdepartamento_id`),
  CONSTRAINT `fk_departamento_id_doc` FOREIGN KEY (`departamento_id`) REFERENCES `departamentos` (`departamento_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_subdepartamento_id_doc` FOREIGN KEY (`subdepartamento_id`) REFERENCES `subdepartamentos` (`subdepartamento_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentos`
--

LOCK TABLES `documentos` WRITE;
/*!40000 ALTER TABLE `documentos` DISABLE KEYS */;
INSERT INTO `documentos` VALUES (1,'http://192.168.1.138:5000/fbdownload/Men%C3%BAs%20y%20submen%C3%BAs%20de%20BDC.docx?_sid=%22qHA9Htf2hjAcs1720NAN165802%22&mode=open&dlink=%222f766964656f2f4d656ec3ba732079207375626d656ec3ba73206465204244432e646f6378%22&stdhtml=true&SynoToken=iHcBf9Ahxo42E','dpto de prueba 5',1,0,'2021-02-12 00:37:05','2021-02-12 00:37:05',NULL),(2,'http://192.168.1.138:5000/fbdownload/Estudio_Habitos_del_Internauta_Mexicano_2014_V_MD.pdf?_sid=%22qHA9Htf2hjAcs1720NAN165802%22&mode=open&dlink=%222f766964656f2f4573747564696f5f48616269746f735f64656c5f496e7465726e617574615f4d65786963616e6f5f323031345f565f4d442e706466%22&stdhtml=true&SynoToken=iHcBf9Ahxo42E','dpto de prueba 5',4,18,'2021-02-12 00:39:30','2021-02-12 00:39:30',NULL),(3,'5bd2cfb6-8a28-45d0-b5b0-43eb3088905b.pdf','documento del formulario',4,18,'2021-03-03 06:57:52','2021-03-03 06:57:52',NULL),(4,'5bd2cfb6-8a28-45d0-b5b0-43eb3088905b.pdf','documento del formulario',4,18,'2021-03-03 06:58:57','2021-03-03 06:58:57',NULL),(6,'5bd2cfb6-8a28-45d0-b5b0-43eb3088905b.pdf','documento del formulario mark 2',4,18,'2021-03-03 23:54:55','2021-03-03 23:54:55',NULL),(7,'5bd2cfb6-8a28-45d0-b5b0-43eb3088905b.pdf','documento del formulario mark 2',4,18,'2021-03-04 00:05:49','2021-03-04 00:05:49',NULL),(132,'AC-001-USO-DEL-FORMATO-APLICACION-CONTABLE-DE-FACTURAS.pdf','AC-001 USO DEL FORMATO APLICACION CONTABLE DE FACTURAS',2,0,NULL,NULL,NULL),(133,'AC-003-ENVIOS-DE-PEDIDOS-Y-MENSAJES-POR-CORREO-ELECTRONICO-A-TRAVES-DEL-SISTEMA-AS400.pdf','AC-003 ENVIOS DE PEDIDOS Y MENSAJES POR CORREO ELECTRONICO A TRAVES DEL SISTEMA AS400',2,0,NULL,NULL,NULL),(134,'AC-004-CONOCIMIENTOS-BÁSICOS-DEL-SISTEMA-CENTRAL-DE-CÓMPUTO.pdf','AC-004 CONOCIMIENTOS BÁSICOS DEL SISTEMA CENTRAL DE CÓMPUTO',2,0,NULL,NULL,NULL),(135,'AC-005-CAMBIO-DE-CONTRASENA.pdf','AC-005 CAMBIO DE CONTRASENA',2,0,NULL,NULL,NULL),(136,'AC-006-ENVIO-DE-MENSAJES-A-TRAVÉS-DEL-SISTEMA.pdf','AC-006 ENVIO DE MENSAJES A TRAVÉS DEL SISTEMA',2,0,NULL,NULL,NULL),(137,'AC-007-POLÍTICA-DE-DEVOLUCIONES-PARA-EMPLEADOS.pdf','AC-007 POLÍTICA DE DEVOLUCIONES PARA EMPLEADOS',2,0,NULL,NULL,NULL),(138,'AC-008-Manejo-de gratificacion-por-detencion-de-farderos-y-robo-interno.pdf','AC-008 Manejo de gratificacion por detencion de farderos y robo interno',2,0,NULL,NULL,NULL),(139,'AC-009-Política-de-descuentos-a-empleados-en-restaurantes.pdf','AC-009 Política de descuentos a empleados en restaurantes',2,0,NULL,NULL,NULL),(140,'AC-010-Mision-Vision-Valores-de-Cimaco.pdf','AC-010 Mision, Vision y Valores de Cimaco',2,0,NULL,NULL,NULL),(141,'AC-011-Politica-de-prohibicion-de-relaciones-extramaritales.pdf','AC-011 Politica de prohibicion de relaciones extramaritales',2,0,NULL,NULL,NULL),(142,'AC-013-Formato-de-Gastos-de-Viaje-Excel.xls','AC-013 Formato de Gastos de Viaje Excel',2,0,NULL,NULL,NULL),(143,'AC-013-Politica-de-Gastos-de-Viaje.pdf','AC-013 Politica de Gastos de Viaje',2,0,NULL,NULL,NULL),(144,'AC-014-Procedimiento-para-valija-interna-Cimaco.pdf','AC-014 Procedimiento para valija interna Cimaco',2,0,NULL,NULL,NULL),(145,'AC-015-Manual-de-procedimientos-de-traspaso.pdf','AC-015 Manual de procedimientos de traspaso',2,0,NULL,NULL,NULL),(146,'AC-016-Politica-para-el-disfrute-de-vacaciones.pdf','AC-016 Politica para el disfrute de vacaciones',2,0,NULL,NULL,NULL),(147,'AC-017-Caracteristicas-de-seguridad-de-la-credencial-de-elector.pdf','AC-017 Caracteristicas de seguridad de la credencial de elector',2,0,NULL,NULL,NULL),(148,'AC-019-Protocolo-de-seguridad-para-contratistas.pdf','AC-019 Protocolo de seguridad para contratistas',2,0,NULL,NULL,NULL),(149,'AC-020 Manual de-reaccion-ante-asaltos-y-activacion-de-alarmas.pdf','AC-020 Manual de reaccion ante asaltos y activacion de alarmas',2,0,NULL,NULL,NULL),(150,'AC-021-Politica-de-consumo-de-alimentos-en-restaurantes-de-Cimaco.pdf','AC-021 Politica de consumo de alimentos en restaurantes de Cimaco',2,0,NULL,NULL,NULL),(151,'AC-023-Politica-para-el-uso-de-unidades-automotrices-y-motocicletas.pdf','AC-023 Politica para el uso de unidades automotrices y motocicletas',2,0,NULL,NULL,NULL),(152,'AC-024-Politica-de-uso-de-la-Ley-Federal-para-la-Prevencion-e-Identificacion-de-Operaciones-con-Recursos-de-Procedencia-Ilicita-para-Cimaco.pdf','AC-024 Politica de uso de la Ley Federal para la Prevencion e Identificacion de Operaciones con Recursos de Procedencia Ilicita para Cimaco',2,0,NULL,NULL,NULL),(153,'AC-025-Procedimiento-de-salidas-de-uso-interno.pdf','AC-025 Procedimiento de salidas de uso interno',2,NULL,NULL,NULL,NULL),(154,'AC-027-Procedimiento-para-enviar-recibo-de-nomina-al-correo-electronico.pdf','AC-027 Procedimiento para enviar recibo de nomina al correo electronico',2,NULL,NULL,NULL,NULL),(155,'AC-028-Reglamento-de-Estacionamiento-de-Empleados-Cimaco-Hidalgo.pdf','AC-028 Reglamento de Estacionamiento de Empleados Cimaco-Hidalgo',2,0,NULL,NULL,NULL),(156,'AC-029-Politíca-para-liquidación-de-mercancías-a-empleados.pdf','AC-029 Politíca para liquidación de mercancías a empleados.',2,0,NULL,NULL,NULL),(157,'AC-030-Politica-Prohibicion-del-Uso-de-Telefonos-Celulares-y-Audífonos-en-Areas-de-Trabajo.pdf','AC-030 Politica Prohibicion del Uso de Telefonos Celulares y Audífonos en Areas de Trabajo.',2,0,NULL,NULL,NULL),(158,'AC-031-Codigo-de-Etica-en-Nuestras-Relaciones-Comerciales.pdf','AC-031 Codigo de Etica en Nuestras Relaciones Comerciales',2,0,NULL,NULL,NULL),(159,'AC-032-Guia-rapida-de-Integracion-de-Personal.pdf','AC-032 Guia rapida de Integracion de Personal',2,0,NULL,NULL,NULL),(160,'AC-033-Manejo-y-Políticas-del-Sistema-Tress.pdf','AC-033 Manejo y Políticas del Sistema Tress',2,0,NULL,NULL,NULL),(161,'AC-034-Instructivo-para-visualizar-los-recibos-de nóminas.pdf','AC-034 Instructivo para visualizar los recibos de nóminas',2,0,NULL,NULL,NULL),(162,'AC-035-MANEJO-DE-MUSICA-AMBIENTAL-EN-TIENDAS-Y-RESTAURANTES.pdf','AC-035 MANEJO DE MUSICA AMBIENTAL EN TIENDAS Y RESTAURANTES',2,0,NULL,NULL,NULL),(163,'AC-036-POLITICA-DE-CRÉDITO-CIMACO-PARA-EMPLEADOS.pdf','AC-036 POLITICA DE CRÉDITO CIMACO PARA EMPLEADOS',2,0,NULL,NULL,NULL),(164,'AC-037-Protocolo-de-operción en-fraudes-con-tarjeta-Cimaco-robada-a-clientes.pdf','AC-037 Protocolo de operción en fraudes con tarjeta Cimaco robada a clientes',2,0,NULL,NULL,NULL),(165,'AC-038-PROTOCOLO-DE-ACTUACIÓN-EN-CASO-DETECCIÓN-DE-ROBO-INTERNO.pdf','AC-038 PROTOCOLO DE ACTUACIÓN EN CASO DETECCIÓN DE ROBO INTERNO',2,0,NULL,NULL,NULL),(166,'AC-039-POLITICA-DE-PREVENCIÓN-Y-ATENCIÓN-AL-ACOSO-SEXUAL-Y-LABORAL.pdf','AC-039 POLITICA DE PREVENCIÓN Y ATENCIÓN AL ACOSO SEXUAL Y LABORAL',2,0,NULL,NULL,NULL),(167,'AC-040-Politica-de-penalizacion-por-Faltantes-de-inventario.pdf','AC-040 Politica de penalizacion por Faltantes de inventario',2,0,NULL,NULL,NULL),(168,'Curso-NOM-035-para-Gerentes-y-Mandos-Medios.pdf','Curso NOM-035 para Gerentes y Mandos Medios',2,0,NULL,NULL,NULL),(169,'Formato-Gastos-de-Viaje-Open-Office.ods','Formato Gastos de Viaje Open Office',2,0,NULL,NULL,NULL),(170,'WhatsApp Image 2021-02-15 at 10.54.52 AM.pdf','gggg',4,18,'2021-03-05 02:58:13','2021-03-05 02:58:13',NULL),(174,'AL-001-REPARTO-DE-MERCANCÍA-A-TRAVÉS-DE-LOS-ALMACENES-DE-TIE.pdf','AL-001-REPARTO-DE-MERCANCÍA-A-TRAVÉS-DE-LOS-ALMACENES-DE-TIE',1,12,NULL,NULL,NULL),(175,'AL-003-ELABORACIÓN-Y-ACTUALIZACIÓN-DE-TRASPASOS-ENTRE-ALMACE.pdf','AL-003-ELABORACIÓN-Y-ACTUALIZACIÓN-DE-TRASPASOS-ENTRE-ALMACE',1,12,NULL,NULL,NULL),(176,'AL-004-MANUAL-DE-TRASPASOS-ENTRE-TIENDAS-Y-ALMACENES-PASANDO-POR-C.D..pdf','AL-004-MANUAL-DE-TRASPASOS-ENTRE-TIENDAS-Y-ALMACENES-PASANDO-POR-C.D.',1,12,NULL,NULL,NULL),(177,'AL-005-Colocación-de-Etiquetas-de-Identificación-para-Cimaco-en-Línea.pdf','AL-005-Colocación-de-Etiquetas-de-Identificación-para-Cimaco-en-Línea',1,12,NULL,NULL,NULL),(178,'CA-002-Operacion-de-la-capacitacion.pdf','CA-002-Operacion-de-la-capacitacion',1,11,NULL,NULL,NULL),(179,'CA-004-Entrenamiento-a-personal-de-ventas-de-nuevo-ingreso.pdf','CA-004-Entrenamiento-a-personal-de-ventas-de-nuevo-ingreso',1,11,NULL,NULL,NULL),(180,'CA-005-Catalogo-de-cursos-institucionales.pdf','CA-005-Catalogo-de-cursos-institucionales',1,11,NULL,NULL,NULL);
/*!40000 ALTER TABLE `documentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subdepartamentos`
--

DROP TABLE IF EXISTS `subdepartamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subdepartamentos` (
  `subdepartamento_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`subdepartamento_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subdepartamentos`
--

LOCK TABLES `subdepartamentos` WRITE;
/*!40000 ALTER TABLE `subdepartamentos` DISABLE KEYS */;
INSERT INTO `subdepartamentos` VALUES (1,'Ventas','2021-02-11 05:56:59','2021-02-11 05:56:59',NULL),(2,'Vigilancia','2021-02-11 05:59:51','2021-02-11 05:59:51',NULL),(3,'Reparto','2021-02-11 06:00:41','2021-02-11 06:00:41',NULL),(4,'Marcaje','2021-02-11 06:00:41','2021-02-11 06:00:41',NULL),(5,'Crédito y Cobranza','2021-02-11 06:00:41','2021-02-11 06:00:41',NULL),(6,'CIP','2021-02-11 06:00:41','2021-02-11 06:00:41',NULL),(7,'Contabilidad','2021-02-11 06:00:41','2021-02-11 06:00:41',NULL),(8,'Compras','2021-02-11 06:00:41','2021-02-11 06:00:41',NULL),(9,'Cajas','2021-02-11 06:00:41','2021-02-11 06:00:41',NULL),(10,'Restaurantes','2021-02-11 06:00:41','2021-02-11 06:00:41',NULL),(11,'Capacitacion','2021-02-11 06:00:41','2021-02-11 06:00:41',NULL),(12,'Almacén','2021-02-11 06:00:41','2021-02-11 06:00:41',NULL),(13,'Manuales Obsoletos','2021-02-11 06:00:41','2021-02-11 06:00:41',NULL),(14,'Proveedores Nacionales','2021-02-11 06:00:41','2021-02-11 06:00:41',NULL),(15,'Proveedores de importación','2021-02-11 06:00:41','2021-02-11 06:00:41',NULL),(16,'Formatos','2021-02-11 06:00:41','2021-02-11 06:00:41',NULL),(17,'Manuales','2021-02-11 06:00:41','2021-02-11 06:00:41',NULL),(18,'Formatos','2021-02-11 06:00:41','2021-02-11 06:00:41',NULL);
/*!40000 ALTER TABLE `subdepartamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `usuario_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `departamento_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `usuario` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`usuario_id`),
  KEY `fk_departamento_id_us_idx` (`departamento_id`),
  CONSTRAINT `fk_departamento_id_us` FOREIGN KEY (`departamento_id`) REFERENCES `departamentos` (`departamento_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','$2y$10$VIlKRAJ7dcgAhZ7VPHiLtuE47PTJNFCWLcCPmH3ejLyxaDL7lV9La',1,'2021-02-20 05:32:35','2021-02-20 05:32:35',NULL,'a073909d42073b97ebaf376a82f307848ea617af','administrador'),(2,'Admin2','$2y$10$dIKshttFkfsqeakDHJewvefKSAW2zzeF8rF8UxZq07ElxD7SJz1O2',1,'2021-02-20 22:32:08','2021-02-20 22:32:08',NULL,'3ea84b839166d89a49dc07dd11253069f39d77d8','administrador2'),(3,'admin@admin.com','$2y$10$fOy1s1LfzEBJiKu7LdJvueUN7RoyElXnuyXJ/BHYWw2Dz37a79Q2S',1,'2021-02-20 22:33:29','2021-02-20 22:33:29',NULL,'74a0d51708180fda5fc016555d069aa8757a656d','admin'),(4,'test@gg.com','$2y$10$oLZCHTlIiSNIKd/JpIl.3e5sIbMkcd1bcO6G.K4c/fUDulynbwYQC',5,'2021-02-20 23:43:54','2021-02-25 04:49:38',NULL,NULL,'Jorge Gonzalez'),(5,'gamaliel.rendon@cimaco.com.mx','$2y$10$LFduzqkKAoHaoUnMq98ldujavNP0kt60Csx4GoMVKOl.xA.qrzii.',1,'2021-03-04 05:20:23','2021-03-04 05:20:23',NULL,NULL,'Gamaliel');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-04 18:24:50
