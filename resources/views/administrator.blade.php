@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-2" style="margin-top: 4%;"></div>
		<div class="col-md-8" style="margin-top: 4%;">
			<div id="logo" class="text-center">
				<img src="https://aluxiluminacion.com/images_mayoristas/35.png" width="220" height="70"
					class="d-inline-block align-top" alt="" loading="lazy">
				<h1>Base De datos del conocimiento</h1>
				<h3>Bienvenido {{Auth::user()->usuario}}</h3>
				<!-- <h3>Bienvenido {{Auth::user()}}</h3> -->
			</div>
		</div>
		<div class="col-md-2" style="margin-top: 4%;"><a href="/" type="submit" class="btn btn-primary">Regresar</a></div>
	</div>

	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<nav>
				<div class="nav nav-tabs" id="nav-tab" role="tablist">
					<a class="nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
						aria-controls="nav-home" aria-selected="true">Cambiar contraseña</a>
					<a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
						aria-controls="nav-profile" aria-selected="false">documentos</a>
					<a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-update" role="tab"
						aria-controls="nav-profile" aria-selected="false">Editar documentos</a>
					<a class="nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab"
						aria-controls="nav-contact" aria-selected="false">Informacion del usuario</a>
				</div>
			</nav>
			<div class="tab-content" id="nav-tabContent">


				<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
					<form action="/updatepass/{{Auth::user()->usuario_id}}" method="post" style="margin-top: 4%;" id="up-pass">
						<div class="form-group">
							<h4>Actualizar contraseña</h4>
							<label for="exampleInputEmail1">Nueva contraseña</label>
							<input name="password" type="password" class="form-control" id="password">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Repita contraseña</label>
							<input name="confirmpassword" type="password" class="form-control" id="confirmpassword">
						</div>
						<button disabled id="btnpass" type="submit" class="btn btn-primary">Actualizar contraseña</button>
						<!-- <div class="registrationFormAlert" style="color:green;" id="CheckPasswordMatch"> -->
					</form>
				</div>

				<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
					<form action="/updoc" method="post" style="margin-top: 4%;" id="up-doc" enctype="multipart/form-data">
						{{ csrf_field() }}





						<div class="form-group">
							<h4>Subir Documentos</h4>
							<label for="exampleInputEmail1">Nombre del documento</label>
							<input type="text" class="form-control" id="nombre" name="nombre">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Documento</label>
							<input type="file" name="file" class="form-control-file" id="ruta">
						</div>
						<div class="form-group">
							<label for="Departamentos">Departamento</label>
							<select class="form-control" id="departamento_id" name="departamento_id">
								<option selected disabled>seleccione una opcion</option>

								@isset($departamentos)
								@if(count($departamentos))
								@foreach($departamentos->where('departamento_id', Auth::user()->departamento_id) as $departamento)
								<option value="{{$departamento->departamento_id}}" name="departamento_id">{{$departamento->nombre}}
								</option>
								@endforeach
								@else
								<option>sin opciones</option>
								@endif
								@endisset

							</select>
						</div>

						<div class="form-group">
							<label for="Subdepartamentos">Subdepartamento</label>
							<select class="form-control" id="subdepartamento_id" name="subdepartamento_id">
								<option selected disabled>seleccione una opcion</option>
								@isset($subdepartamentos)
								@if(count($subdepartamentos))
								@foreach($subdepartamentos as $subdepartamento)
								<option value="{{$subdepartamento->subdepartamento_id}}">{{$subdepartamento->nombre}}</option>
								@endforeach
								@else
								<option>sin opciones</option>
								@endif
								@endisset
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Subir documento</button>
					</form>
				</div>

				<div class="tab-pane fade" id="nav-update" role="tabpanel" aria-labelledby="nav-profile-tab">
					<table class="table">
						<thead>
							<tr>
								<th scope="col">Archivo</th>
								<th scope="col">Fecha de publicacion</th>
								<th scope="col">Acciones</th>
							</tr>
						</thead>

						<tbody>

							@isset($documentos)
							@if(count($documentos))
							@foreach($documentos->where('departamento_id', Auth::user()->departamento_id) as $documento)
							<tr>

								<td>
									<a target="_blank" style="text-overflow: ellipsis;overflow: hidden;"
										href="/storage/{{$documento->ruta}}">{{$documento->nombre}}</a>

								</td>
								<td>{{ date("d M Y", strtotime($documento->created_at)) }}</td>
								<td>
									<button type="button" onclick="getVal(this.value)" id="edit-data" value="{{$documento->documento_id}}"
										class="btn btn-primary btn-sm">Editar</button>
								</td>
							</tr>
							@endforeach
							@else
							<div class="col-12 mb-5">
								<div class="card card-view">
									<div class="card-body">
										<p class="page-subtitle text-center">No hay Documentos</p>
									</div>
								</div>
							</div>
							@endif
							@endisset

						</tbody>
					</table>
					<form style="margin-top: 4%;margin-bottom: 2%;" id="update-doc" method="post" action="/updatedoc" 
					enctype="multipart/form-data">
						{{ csrf_field() }}
						
						<input type="hidden" id="documento_id_up" name="documento_id_up">
						<div class="form-group">
							<h4>Editar Documentos</h4>
							<label for="exampleInputEmail1">Nombre del documento</label>
							<input type="text" class="form-control" id="nombre_up" name="nombre_up">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Documento</label>
							<input type="file" name="ruta" class="form-control-file" id="ruta">
						</div>
						<div class="form-group">
							<label for="Departamentos">Departamento</label>
							<select class="form-control" id="departamento_id" name="departamento_id">
								<option selected disabled>seleccione una opcion</option>

								@isset($departamentos)
								@if(count($departamentos))
								@foreach($departamentos->where('departamento_id', Auth::user()->departamento_id) as $departamento)
								<option value="{{$departamento->departamento_id}}" name="departamento_id" id="departamento_id_up">
									{{$departamento->nombre}}
								</option>
								@endforeach
								@else
								<option>sin opciones</option>
								@endif
								@endisset

							</select>
						</div>


						<button type="submit" id="" class="btn btn-primary">Subir documento</button>
					</form>
				</div>

				<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
					<form action="/upname/{{Auth::user()->usuario_id}}" method="post" style="margin-top: 4%;">
						<div class="form-group">
							<h4>Actualizar informacion del usuario</h4>
							<label for="exampleInputEmail1">Nombre completo</label>
							<input type="text" class="form-control" id="usuario" name="usuario" value="{{Auth::user()->usuario}}">
						</div>
						<div class="form-group">
							<label for="exampleFormControlSelect1">Departamento Actual</label>
							<select class="form-control" id="departamento_id" name="departamento_id">
								<option value="{{Auth::user()->departamento_id}}" selected disabled>seleccione una opcion</option>

								@isset($departamentos)
								@if(count($departamentos))
								@foreach($departamentos as $departamento)
								<option value="{{$departamento->departamento_id}}">{{$departamento->nombre}}</option>
								@endforeach
								@else
								<option>sin opciones</option>
								@endif
								@endisset

							</select>
						</div>
						<button type="submit" class="btn btn-primary">Actualizar Usuario</button>
					</form>
				</div>


			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript">

	function getVal(value) {
		console.log('gggg', value)
		var id = value;
		$.ajax({
			type: 'GET',
			url: '{{url("/documentosfilter")}}' + '/' + id,
			dataType: 'json',
			success: function (response) {
				data = response;
				localStorage.setItem("data", response.data.documentos.documento_id);
				console.log('data de todo', response.data.documentos);
				$("#nombre_up").val(response.data.documentos.nombre);
				$('#documento_id_up').val(id);
				// $("#file").val(response.data.documentos.ruta);
			},
			error: function () {
				alert('Error occured');
			}
		});


	}

	function sendVal(value) {
		console.log('gggg', value)
		var id = value;

		$.ajax({
			type: 'GET',
			url: '{{url("/documentosfilter")}}' + '/' + id,
			dataType: 'json',
			success: function (response) {
				data = response;
				localStorage.setItem("data", data);
				console.log('data de todo', response.data.documentos);
				$("#nombre_up").val(response.data.documentos.nombre);
				// $("#file").val(response.data.documentos.ruta);
			},
			error: function () {
				alert('Error occured');
			}
		});


	}

	function checkPasswordMatch() {
		var password = $("#password").val();
		var confirmPassword = $("#confirmpassword").val();
		if (password != confirmPassword)
			// $("#CheckPasswordMatch").html("Passwords does not match!");
			$("#btnpass").prop('disabled', true);
		else
			// $("#CheckPasswordMatch").html("Passwords match.");
			$("#btnpass").prop('disabled', false);
	}
	$(document).ready(function () {
		$("#confirmpassword").keyup(checkPasswordMatch);
	});

	// $("#Enviar_gg").click(function () {
	// 	// e.preventDefault();
	// 	id = localStorage.getItem("data");
	// 	var id = id;
	// 	var nombre = $('#nombre_up').val();
	// 	var file = $('#file').val();
	// 	var departamento_id = $('#departamento_id_up').val();
	// 	var data = {
	// 		nombre: nombre,
	// 		file: file,
	// 		departamento_id: departamento_id
	// 	};
	// 	console.log('datos a enviar', data)
	// 	$.ajax({
	// 		type: "POST",
	// 		url: '{{url("/updatedoc")}}' + '/' + id,
	// 		// processData: false,  // tell jQuery not to process the data
	// 		// contentType: false,
	// 		data: data,
	// 		success: function (response) {
	// 			console.log('respuesta', response)
	// 			// alert("Se ha realizado el POST con exito "+msg);
	// 		}
	// 	});
	// });

</script>



@endsection